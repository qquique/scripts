## wireguard mkvpn.sh

Needs:

- [ ] Modified .conf from wireguard provider /etc/wireguard/wgns.conf

```
[Interface]
PrivateKey = <PrivateKey>

[Peer]
PublicKey = <PublicKey>
AllowedIPs = 0.0.0.0/0,::0/0
Endpoint = IP:PORT
```

- [ ] netns configuration to set another dns instead of provided in .conf /etc/netns/vpnwg/resolv.conf

```
nameserver 1.1.1.1
```

- [ ] After creating vpn, to launch a program to use in the vpn namespace define the following shell function ~/.zshrc, restart session

```
vpnify() { sudo -E ip netns exec vpnwg sudo -E -u \#$(id -u) -g \#$(id -g) "$@"; }
```

- [ ] program can be run with

```
vpnify firefox
```
