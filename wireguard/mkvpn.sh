#!/bin/bash

# network namespace name
NETNS_NAME="vpnwg"
# modified /etc/wireguard/wgns.conf
DEV_NAME="wgns"
# ipv4 assigned on conf
IPV4_ADDR="10.68.203.66/32"
# ipv6 assigned on conf
IPV6_ADDR="fc00:bbbb:bbbb:bb01::5:cb41/128"
# mtu
MTU=1380

# Create a Wireguard network interface in the default namespace.
sudo ip link add $DEV_NAME type wireguard
# Load the Wireguard configuration.
sudo wg setconf $DEV_NAME /etc/wireguard/$DEV_NAME.conf
# Create a new network namespace.
sudo ip netns add $NETNS_NAME
# up lo (127.0.0.1)
sudo ip netns exec $NETNS_NAME ip link set dev lo up
# Move the Wireguard interface to the network namespace.
sudo ip link set $DEV_NAME netns $NETNS_NAME
# set mtu
sudo ip -n $NETNS_NAME link set mtu $MTU dev $DEV_NAME
# Set the IP address of the Wireguard interface.
sudo ip -n $NETNS_NAME addr add $IPV4_ADDR dev $DEV_NAME
sudo ip -6 -n $NETNS_NAME addr add $IPV6_ADDR dev $DEV_NAME
# Bring up the Wireguard interface.
sudo ip -n $NETNS_NAME link set $DEV_NAME up
# Make the Wireguard interface the default route.
sudo ip -n $NETNS_NAME route add default dev $DEV_NAME
sudo ip -n $NETNS_NAME -6 route add default dev $DEV_NAME
