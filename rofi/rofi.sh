#!/usr/bin/zsh
rofi -combi-modi "window,drun,run" -display-combi ">> " -matching regex -show combi -location 0 -lines 10 -bw 0 -font "JetBrains Mono 9" -separator-style none -icon-theme "ePapirus" -show-icons -opacity 85 -theme "Arc-Dark"
