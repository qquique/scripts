#!/usr/bin/env bash
declare -A LEVELS

LEVELS=(
    ["performance"]="performance"
    ["balanced"]="balanced"
    ["power-saver"]="power-saver"
)

gen_list() {
	for i in "${!LEVELS[@]}"
	do
		echo "$i"
	done
}

main() {
	  selected=$( (gen_list) | rofi -dmenu -no-custom -location 0 -lines 8 -bw 0 -font "Terminus Medium 9" -separator-style none -opacity 85 -theme-str "@theme \"Adapta-Nokto\"" -p "Power Profile > " )
	if [[ -n "$selected" ]]; then
		level=${LEVELS[$selected]}
    		powerprofilesctl set "$level"
	else
		exit
	fi
}

main

exit 0


